# Keywords for flypapers on Mastodon

The keywords listed in file `tag-drosophila.txt` will be used to search titles and abstracts of all papers tooted by [@flypapers@botsin.space](https://botsin.space/web/@flypapers). Any keyword matching will be added as a tag to the toot.

You can fork (*) this repository, edit the file and then make a pull/merge request to suggest new keywords. 
Ready? [Click here to open the file and start editing it](https://gitlab.com/-/ide/project/jocelyn_etienne/keywords-for-flypapers-on-mastodon/edit/main/-/tag-drosophila.txt) ...and let Gitlab guide you.

## How to enter my keywords

Keywords are searched in a case insensitive, whole-word match. If a keyword is commonly used in several forms, e.g. singular and plural, noun and adjective..., you can add all on the same line with a `|`: `larva|larvae|larval`. Only one of the forms will be tagged.

## Additional help

**(*) Fork**
In order to fork, you need to create a gitlab account and be logged in.
Then [follow instructions](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
